import React from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

import Routes from '~/routes'
import history from '~/services/history'
import { store } from '~/store'

import GlobalStyles from './styles/global'

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <GlobalStyles />
      <Routes />
      <ToastContainer autoClose={3000} />
    </Router>
  </Provider>
)

export default App
