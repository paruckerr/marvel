import React from 'react'

import {
  Grid,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from '@material-ui/core'
import t from 'prop-types'

const style = {
  height: 220,
}

function Serie({ serie }) {
  return (
    <Grid item sm={6} xs={12} xl={4} md={4} lg={3}>
      <Card>
        <CardActionArea>
          <CardMedia
            style={style}
            image={`${serie?.thumbnail.path}.${serie?.thumbnail.extension}`}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h5">
              {serie.startYear} - {serie.endYear}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {serie.title}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}

Serie.propTypes = {
  serie: t.objectOf(t.any).isRequired,
}

export default Serie
