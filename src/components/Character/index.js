import React from 'react'

import {
  Grid,
  Card,
  Button,
  CardMedia,
  CardContent,
  CardActions,
  CardActionArea,
} from '@material-ui/core'
import t from 'prop-types'

const style = {
  height: 220,
}

function Character({ data, onClick }) {
  return (
    <Grid item sm={6} xs={12} xl={4} md={4} lg={3}>
      <Card>
        <CardActionArea>
          <CardMedia
            style={style}
            image={`${data?.thumbnail.path}.${data?.thumbnail.extension}`}
          />
          <CardContent>
            <CardActions>
              <Button onClick={() => onClick(data.id)} size="small">
                {data.name}
              </Button>
            </CardActions>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}

Character.propTypes = {
  data: t.objectOf(t.any).isRequired,
}

export default Character
