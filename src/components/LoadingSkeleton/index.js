import React from 'react'

import {
  Grid,
  Card,
  CardContent,
  CardActions,
  CardActionArea,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'

const style = {
  height: 220,
}

function LoadingSkeleton() {
  return [1, 2, 3, 4, 6, 7, 8, 9, 10].map((i) => (
    <Grid item sm={6} xs={12} xl={4} md={4} lg={3} key={String(i)}>
      <Card>
        <CardActionArea>
          <Skeleton animation="wave" variant="rect" style={style} />
          <CardContent>
            <CardActions>
              <Skeleton
                animation="wave"
                height={10}
                style={{ marginBottom: 6 }}
              />
              <Skeleton animation="wave" height={10} width="80%" />
            </CardActions>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  ))
}

export default LoadingSkeleton
