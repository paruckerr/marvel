import React from 'react'

import { Grid, Button, Avatar, Typography } from '@material-ui/core'
import { EditOutlined } from '@material-ui/icons'
import t from 'prop-types'

const style = {
  content: {
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 150,
    height: 150,
  },
}

function Character({ data, open }) {
  return (
    <Grid xl={12} style={style.content}>
      <Avatar
        style={style.avatar}
        src={`${data?.thumbnail?.path}.${data?.thumbnail?.extension}`}
      />
      <Typography gutterBottom variant="h3" component="h3">
        {data?.name}
      </Typography>
      <Typography gutterBottom variant="subtitle2" component="p">
        {data?.description}
      </Typography>
      <Button
        onClick={open}
        variant="contained"
        color="primary"
        startIcon={<EditOutlined />}
      >
        Editar
      </Button>
    </Grid>
  )
}

Character.propTypes = {
  data: t.objectOf(t.any).isRequired,
}

export default Character
