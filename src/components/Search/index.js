import React from 'react'

import { TextField } from '@material-ui/core'
import t from 'prop-types'

const style = {
  display: 'flex',
  padding: 20,
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  flexDirection: 'row',
}

const styleInput = {
  color: 'white',
  marginRight: 4,
}

function Search({ value, onChange, onSearch }) {
  return (
    <form onSubmit={(e) => onSearch(e)} style={style}>
      <TextField
        fullWidth
        type="search"
        value={value}
        style={styleInput}
        variant="outlined"
        placeholder="Busque o seu personagem..."
        onChange={(e) => onChange(e.target.value)}
      />
    </form>
  )
}

Search.propTypes = {
  value: t.string.isRequired,
  onChange: t.func.isRequired,
  onSearch: t.func.isRequired,
}

export default Search
