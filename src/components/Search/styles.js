import styled from 'styled-components'

export const SearchInput = styled.input`
  width: 100px;
  border-radius: 10px;
  padding: 5px;
`
