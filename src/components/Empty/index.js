import React from 'react'

import { Typography, Button } from '@material-ui/core'
import { SearchOutlined } from '@material-ui/icons'

const style = {
  padding: 20,
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
}

export default function Empty({ onClick }) {
  return (
    <div style={style}>
      <SearchOutlined />
      <Typography>Nenhum resultado encontrado...</Typography>
      <Button onClick={() => onClick()}>Voltar</Button>
    </div>
  )
}
