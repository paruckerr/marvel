import React from 'react'

import { Typography, Grid, Button } from '@material-ui/core'

const style = {
  text: {
    marginTop: 20,
    marginBottom: 20,
  },
  content: {
    padding: 10,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
}

export default function Bottom({ onClick, loading, paginationText }) {
  return (
    <Grid content justify="center" xl={12} style={style.content}>
      <Typography style={style.text}>{paginationText}</Typography>
      <Button
        variant="contained"
        color="secondary"
        onClick={onClick}
        disabled={loading}
      >
        Carregar mais...
      </Button>
    </Grid>
  )
}
