import { createGlobalStyle } from 'styled-components'
import 'react-toastify/dist/ReactToastify.css'

export default createGlobalStyle`
  body {
    background-color: #f1f2f3!important;
  }
`
