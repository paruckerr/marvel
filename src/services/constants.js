import md5 from 'md5'
import qs from 'qs'

const ts = Date.now()
// const hash = md5(`${ts}${REACT_APP_PRIVATE_KEY}${REACT_APP_PUBLIC_KEY}`)
// export default qs.stringify({
//   ts,
//   apikey: REACT_APP_PUBLIC_KEY,
//   hash,
// })
const hash = md5(
  `${ts}${process.env.REACT_APP_PRIVATE_KEY}${process.env.REACT_APP_PUBLIC_KEY}`
)
export default qs.stringify({
  ts,
  apikey: process.env.REACT_APP_PUBLIC_KEY,
  hash,
})
