import { persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'

import createStore from './createStore'
import rootReducer from './ducks/rootReducer'
import rootSaga from './ducks/rootSaga'

const sagaMonitor = null
const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
const middlewares = [sagaMiddleware]
const store = createStore(rootReducer, middlewares)
const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)

export { store, persistor }
