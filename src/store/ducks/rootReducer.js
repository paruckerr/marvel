import { combineReducers } from 'redux'

import character from './character/reducer'
import series from './characteres-series/reducer'
import characteres from './characteres/reducer'

export default combineReducers({
  characteres,
  series,
  character,
})
