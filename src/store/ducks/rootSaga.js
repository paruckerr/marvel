import { all } from 'redux-saga/effects'

import character from './character/sagas'
import series from './characteres-series/sagas'
import characteres from './characteres/sagas'

export default function* rootSaga() {
  return yield all([characteres, series, character])
}
