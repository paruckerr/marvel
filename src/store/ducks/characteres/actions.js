import { CHARACTERES, CHARACTERES_FAIL, CHARACTERES_SUCCESS } from './types'

export function getCharacteres(query) {
  return {
    type: CHARACTERES,
    payload: query,
  }
}

export function characteresSuccess(data) {
  return {
    type: CHARACTERES_SUCCESS,
    payload: { data },
  }
}

export function characteresFail() {
  return {
    type: CHARACTERES_FAIL,
  }
}
