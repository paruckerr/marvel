import { toast } from 'react-toastify'

import { all, put, call, takeLatest } from 'redux-saga/effects'

import api from '~/services/api'
import token from '~/services/constants'

import { characteresSuccess, characteresFail } from './actions'
import { CHARACTERES } from './types'

export function* listCharacteres(query) {
  try {
    const res = yield call(api.get, `characters?${token}${query.payload || ''}`)

    yield put(characteresSuccess(res.data.data))
  } catch (error) {
    toast.error(error)
    yield put(characteresFail())
  }
}

export default all([takeLatest(CHARACTERES, listCharacteres)])
