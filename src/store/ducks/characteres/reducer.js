import { produce } from 'immer'

import {
  CHARACTERES,
  CHARACTERES_FAIL,
  CHARACTERES_SUCCESS,
} from '~/store/ducks/characteres/types'

const INITIAL_STATE = {
  data: {},
  query: '',
  error: false,
  loading: false,
  success: false,
}

export default function characteres(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHARACTERES:
      return produce(state, (draft) => {
        draft.loading = true
        draft.query = action.payload
      })
    case CHARACTERES_SUCCESS:
      return produce(state, (draft) => {
        draft.data = action.payload.data
        draft.loading = false
        draft.success = true
      })
    case CHARACTERES_FAIL:
      return produce(state, (draft) => {
        draft.loading = false
        draft.error = true
      })
    default:
      return state
  }
}
