import { produce } from 'immer'

import {
  CHARACTERES_SERIES,
  CHARACTERES_SERIES_FAIL,
  CHARACTERES_SERIES_SUCCESS,
} from './types'

const INITIAL_STATE = {
  id: '',
  data: {},
  error: false,
  loading: false,
  success: false,
}

export default function series(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHARACTERES_SERIES:
      return produce(state, (draft) => {
        draft.loading = true
        draft.id = action.id
      })
    case CHARACTERES_SERIES_SUCCESS:
      return produce(state, (draft) => {
        draft.data = action.payload.data
        draft.loading = false
        draft.success = true
      })
    case CHARACTERES_SERIES_FAIL:
      return produce(state, (draft) => {
        draft.loading = false
        draft.error = true
      })
    default:
      return state
  }
}
