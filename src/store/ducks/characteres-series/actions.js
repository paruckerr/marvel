import {
  CHARACTERES_SERIES,
  CHARACTERES_SERIES_FAIL,
  CHARACTERES_SERIES_SUCCESS,
} from './types'

export function getSeries(id) {
  return {
    id,
    type: CHARACTERES_SERIES,
  }
}

export function characteresSuccess(data) {
  return {
    type: CHARACTERES_SERIES_SUCCESS,
    payload: { data },
  }
}

export function characteresFail() {
  return {
    type: CHARACTERES_SERIES_FAIL,
  }
}
