import { toast } from 'react-toastify'

import { all, put, call, takeLatest } from 'redux-saga/effects'

import api from '~/services/api'
import token from '~/services/constants'

import { characteresSuccess, characteresFail } from './actions'
import { CHARACTERES_SERIES } from './types'

export function* getSeriesById(data) {
  try {
    const res = yield call(api.get, `characters/${data.id}/series?${token}`)

    yield put(characteresSuccess(res.data.data))
  } catch (error) {
    toast.error(error)
    yield put(characteresFail())
  }
}

export default all([takeLatest(CHARACTERES_SERIES, getSeriesById)])
