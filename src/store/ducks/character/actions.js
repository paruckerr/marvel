import { CHARACTER, CHARACTER_FAIL, CHARACTER_SUCCESS } from './types'

export function getCharacter(id) {
  return {
    id,
    type: CHARACTER,
  }
}

export function characterSuccess(data) {
  return {
    type: CHARACTER_SUCCESS,
    payload: { data },
  }
}

export function characterFail() {
  return {
    type: CHARACTER_FAIL,
  }
}
