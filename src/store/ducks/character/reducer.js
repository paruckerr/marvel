import { produce } from 'immer'

import { CHARACTER, CHARACTER_SUCCESS, CHARACTER_FAIL } from './types'

const INITIAL_STATE = {
  data: {},
  id: '',
  error: false,
  loading: false,
  success: false,
}

export default function characteres(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHARACTER:
      return produce(state, (draft) => {
        draft.loading = true
        draft.id = action.id
      })
    case CHARACTER_SUCCESS:
      return produce(state, (draft) => {
        draft.data = action.payload.data
        draft.loading = false
        draft.success = true
      })
    case CHARACTER_FAIL:
      return produce(state, (draft) => {
        draft.loading = false
        draft.error = true
      })
    default:
      return state
  }
}
