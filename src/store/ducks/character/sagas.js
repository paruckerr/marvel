import { toast } from 'react-toastify'

import { all, put, call, takeLatest } from 'redux-saga/effects'

import api from '~/services/api'
import token from '~/services/constants'

import { characterSuccess, characterFail } from './actions'
import { CHARACTER } from './types'

export function* getCharacter(data) {
  try {
    const res = yield call(api.get, `characters/${data.id}?${token}`)

    yield put(characterSuccess(res.data.data))
  } catch (error) {
    toast.error(error)
    yield put(characterFail())
  }
}

export default all([takeLatest(CHARACTER, getCharacter)])
