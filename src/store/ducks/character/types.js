export const CHARACTER = '@character/FIND'

export const CHARACTER_SUCCESS = '@character/FIND_SUCCESS'

export const CHARACTER_FAIL = '@character/FIND_FAIL'
