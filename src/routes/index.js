import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Detail from '~/pages/Detail'
import Home from '~/pages/Home'
import NotFound from '~/pages/NotFound'
import history from '~/services/history'

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/detail/:id" component={Detail} />
    <Route exact path="*" history={history} component={NotFound} />
  </Switch>
)

export default Routes
