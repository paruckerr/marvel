import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'

import { Grid } from '@material-ui/core'
import qs from 'qs'

import Bottom from '~/components/Bottom'
import Character from '~/components/Character'
import Empty from '~/components/Empty'
import LoadingSkeleton from '~/components/LoadingSkeleton'
import Search from '~/components/Search'
import Layout from '~/pages/Layout'
import { getCharacteres } from '~/store/ducks/characteres/actions'

function Home() {
  const history = useHistory()
  const dispatch = useDispatch()
  const characteres = useSelector((state) => state.characteres)

  const [query, setQuery] = useState({ name: '' })
  const [items, setItems] = useState([])
  const [total, setTotal] = useState(0)
  const [loading, setLoading] = useState(true)
  const [loadingMore, setLoadingMore] = useState(false)
  const [pagination, setPagination] = useState({
    count: 20,
    limit: 20,
    offset: 0,
  })

  function onChange(value) {
    return setQuery({ ...query, name: value })
  }

  function onClick(id) {
    return history.push(`detail/${id}`)
  }

  function onSearch(e) {
    e.preventDefault()

    if (!query.name) {
      toast.error('O campo não pode estar vazio!')
      return false
    }

    if (query.name.length < 2) {
      toast.error('Digite pelo menos 2 caracteres!')
      return false
    }

    setLoading(true)

    return dispatch(getCharacteres(`&${qs.stringify(query)}`))
  }

  function onReset() {
    setLoading(true)
    setQuery({ ...query, name: '' })
    return dispatch(getCharacteres())
  }

  function onFetchMore() {
    setLoadingMore(true)
    const newLimit = Number(pagination.limit) + 20
    const newQuery = { limit: newLimit }
    return dispatch(getCharacteres(`&${qs.stringify(newQuery)}`))
  }

  const findCharacteres = useCallback(() => {
    dispatch(getCharacteres())
  }, [dispatch])

  useEffect(() => {
    findCharacteres()
  }, []) //eslint-disable-line

  useEffect(() => {
    if (characteres.success) {
      const { count, limit, offset, results } = characteres.data
      setItems(
        results.map((i) => ({
          ...i,
          image: `${i.thumbnail.path}.${i.thumbnail.extension}`,
        }))
      )
      setTotal(characteres.data.total)
      setPagination({ count, limit, offset })
      setLoading(false)
      setLoadingMore(false)
    }
  }, [characteres])

  return (
    <Layout>
      <Search
        onReset={onReset}
        value={query.name}
        onChange={onChange}
        onSearch={onSearch}
      />

      {loading && (
        <Grid container spacing={2}>
          <LoadingSkeleton />
        </Grid>
      )}

      {!loading && items.length === 0 && <Empty onClick={onReset} />}

      <Grid container spacing={2}>
        {!loading &&
          items.map((character) => (
            <Character
              data={character}
              onClick={onClick}
              key={character.name}
            />
          ))}
      </Grid>

      {!loading && items.length > 0 && (
        <Bottom
          loading={loadingMore}
          onClick={() => onFetchMore()}
          paginationText={`${pagination.count} de ${total}`}
        />
      )}
    </Layout>
  )
}

export default Home
