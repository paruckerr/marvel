import React from 'react'

import { Heading } from '@chakra-ui/core'

import Layout from '~/pages/Layout'

export default function NotFound() {
  return (
    <Layout>
      <Heading>404</Heading>
    </Layout>
  )
}
