import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { Link } from 'react-router-dom'

import { Container, Grid } from '@material-ui/core'

import Logo from '~/assets/images/logo.png'

export default function Layout({ children }) {
  return (
    <Container>
      <Grid container justify="center" xl={12}>
        <Link to="/">
          <LazyLoadImage effect="blur" src={Logo} width="200px" />
        </Link>
      </Grid>

      {children}
    </Container>
  )
}
