import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams, useHistory } from 'react-router-dom'

import { Breadcrumbs, Grid, Link, Divider, Typography } from '@material-ui/core'

import Character from '~/components/CharacterHeader'
import LoadingSkeleton from '~/components/LoadingSkeleton'
import Serie from '~/components/Serie'
import Layout from '~/pages/Layout'
import { getCharacter } from '~/store/ducks/character/actions'
import { getSeries } from '~/store/ducks/characteres-series/actions'

import FormDetail from './components/Modal'

import 'react-lazy-load-image-component/src/effects/blur.css'

const CHARACTER_STORAGE = (id) => `@character:${id}`

function Detail() {
  const { id } = useParams()
  const dispatch = useDispatch()
  const history = useHistory()

  const series = useSelector((state) => state.series)
  const character = useSelector((state) => state.character)

  const [items, setItems] = useState([])
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = useState(true)
  const [loadingSeries, setLoadingSeries] = useState(true)
  const [characterDetail, setCharacterDetail] = useState({})

  function storeData(data) {
    setOpen(false)
    return localStorage.setItem(CHARACTER_STORAGE(id), JSON.stringify(data))
  }

  async function getData(data) {
    const value = await localStorage.getItem(CHARACTER_STORAGE(data.id))

    if (value !== null) {
      return setCharacterDetail(JSON.parse(value))
    }

    return setCharacterDetail(data)
  }

  function onChange(key, value) {
    return setCharacterDetail({ ...characterDetail, [key]: value })
  }

  const loadSeries = useCallback(() => {
    dispatch(getSeries(id))
  }, [dispatch, id])

  const findById = useCallback(() => {
    dispatch(getCharacter(id))
  }, [dispatch, id])

  useEffect(() => {
    loadSeries()
  }, []) //eslint-disable-line

  useEffect(() => {
    findById()
  }, []) //eslint-disable-line

  useEffect(() => {
    if (series.success) {
      setItems(series.data.results)
      setLoadingSeries(false)
    }

    if (character.success) {
      getData(character.data.results[0])
      setLoading(false)
    }
  }, [series, character])

  return (
    <Layout>
      <Grid container xl={12}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/" onClick={() => history.go(-1)}>
            <Typography>Home</Typography>
          </Link>
          <Typography>{characterDetail?.name}</Typography>
        </Breadcrumbs>
      </Grid>

      {!loading && (
        <Character data={characterDetail} open={() => setOpen(true)} />
      )}

      <Divider />

      <Grid container spacing={2} style={{ padding: 10 }}>
        <Grid item xs={12} xl={12}>
          <Grid container justify="flex-start" spacing={2}>
            {loadingSeries && <LoadingSkeleton />}

            {!loadingSeries &&
              items.map((serie, key) => (
                <Serie serie={serie} key={String(key)} />
              ))}
          </Grid>
        </Grid>
      </Grid>

      <FormDetail
        open={open}
        values={characterDetail}
        handleClose={() => setOpen(false)}
        onClick={() => storeData(characterDetail)}
        onChange={onChange}
      />
    </Layout>
  )
}

export default Detail
