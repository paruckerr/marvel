import React from 'react'

import {
  Dialog,
  DialogContent,
  DialogActions,
  TextField,
  Button,
  DialogTitle,
} from '@material-ui/core'

function FormModal({ values, open, onClick, onChange, handleClose }) {
  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="edit-character">
      <DialogContent title="Editar Dados">
        <DialogTitle id="form-dialog-title">Editar Dados</DialogTitle>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Nome"
          type="text"
          placeholder="Nome"
          value={values.name}
          onChange={(e) => onChange('name', e.target.value)}
          fullWidth
        />
        <TextField
          autoFocus
          fullWidth
          type="text"
          margin="dense"
          id="description"
          label="Descrição"
          placeholder="Descrição"
          value={values.description}
          onChange={(e) => onChange('description', e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClick}>Salvar</Button>
      </DialogActions>
    </Dialog>
  )
}

export default FormModal
