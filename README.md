Marvel [Deploy](https://marvel-soft.surge.sh/).

## Para rodar o projeto

Segue comandos.

### `yarn install`

### `yarn start`

## Variáveis de Ambiente

### `.env.development`

```
SKIP_PREFLIGHT_CHECK=true
REACT_APP_PUBLIC_KEY=7305b5950525060483bc3459cd325127
REACT_APP_PRIVATE_KEY=244cd3d4f40a1ea44d62516c0b51ae3cf9e4187e
```
